import re
import ipaddress
import copy
import random
import string
from pprint import pprint

class Router():
    def __init__(self, Brand='Mikrotik', Ports=5, Model='RB2011'):
        self.Model=Model
        self.Brand=Brand
        self.Serial=self.SerialGen()
        self.hostname='localhost'
        self.Ports=Ports
        self.Interface_list= []

        for port in range(Ports):
            Interface=Interfaces()
            Interface.Index=port
            Interface.Ipv4=str(self.Ipv4Gen())
            Interface.Mac=self.SerialGen()
            Interface.Type='Ethernet'
            Interface.Vlan=port+1
            self.Interface_list.append(Interface.__dict__)

    def __str__(self):
        output = []
        for key in self.__dict__:
            if isinstance(key, list):
                for i in range(len(key)):
                    output.append(key[i])
            else:
                output.append('{key}:{value}'.format(key=key, value=self.__dict__[key]))
        return output

    def SerialGen(self):
        size=12
        chars=string.ascii_uppercase+string.digits
        return ''.join([random.choice(chars) for _ in range(size)])

    def Ipv4Gen(self):
        net = ipaddress.ip_network('10.10.10.0/24')
        return net[int(random.randrange(1, 254, 1))]

class Interfaces():
    def __init__(self):
        self.Index=None
        self.Speed=1000
        self.Type=None
        self.Ipv4=None
        self.Vlan=1
        self.Status='Down'
        self.Mac=None

def main():
    router = Router()
    pprint(router.__str__())

if __name__ == '__main__':
    main()
