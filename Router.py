import re
import ipaddress
import copy
from pprint import pprint

class Router(object):
    def __init__(self, ports, serial):
        self.ports=None
        self.ports_list= []
        self.serial=None

        port_prop_list={'Port:':None, 'speed':1000, 'vlan':None , 'ipv4':None}
        pattern = "^[a-zA-Z0-9_]*$"
        if re.match(pattern, serial):
            self.serial=serial
        else:
            raise Exception('Invalid serial '+serial)

        if ports>0<66:
            net = ipaddress.ip_network('65.16.0.1/22', False)
            self.ports=ports
            #List = list()
            for i in range(ports):
                port_prop_list['vlan']=i+4
                port_prop_list['ipv4']=str(net[i+1])
                port_prop_list['Port:']=i
                l = copy.deepcopy(port_prop_list)
                self.ports_list.append(l)
            #print(self.ports_list)


def main():
    router = Router(10, 'EEDAS3487R66')
    pprint(router.__dict__)

if __name__ == '__main__':
    main()
